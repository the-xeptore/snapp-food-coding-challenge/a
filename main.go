package main

import (
	"log"

	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/config"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/order_placer"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/order_placer/store"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/recipient"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/rest"
)

func main() {
	conf, err := config.Load()
	if nil != err {
		log.Fatalln("Error loading configurations:", err.Error())
	}
	log.Printf("%#v", conf)

	mysqlStore, err := store.InitializeMysqlStore(&conf.OrderPlacer.Database)
	if nil != err {
		log.Fatalln("Error while initializing MySQL database connection:", err.Error())
	}

	err = order_placer.InitializeRedisOrderPlacer(&conf.OrderPlacer, &mysqlStore)
	if nil != err {
		log.Fatalln("Error while initializing order placer app:", err.Error())
	}

	redisRecipient, err := recipient.InitializeRedisRecipient(&conf.Recipient)
	if nil != err {
		log.Fatalln("Error while connecting recipient app:", err.Error())
	}

	server, err := rest.Initialize(redisRecipient)
	if nil != err {
		log.Fatalln("Error initializing http server:", err.Error())
	}

	err = server.Start(&conf.Server)
	if nil != err {
		log.Fatalln("Error starting http server:", err.Error())
	}
}

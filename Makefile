.PHONY: build run test

GO = go

build:
	$(GO) build -o app

run: build
	./app

test:
	@echo No tests found.

package recipient

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/models"
)

func (recipient *RedisRecipient) HandleNewOrder(request models.NewOrderForm) error {
	serialized, err := json.Marshal(request)
	if nil != err {
		return errors.New(fmt.Sprintf("Unable to serialize request form: %s", err.Error()))
	}

	err = recipient.queue.PublishBytes(serialized)
	if nil != err {
		return errors.New(fmt.Sprintf("Unable to send request to queue: %s", err.Error()))
	}

	return nil
}

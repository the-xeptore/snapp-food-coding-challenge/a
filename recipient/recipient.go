package recipient

import (
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/models"
)

type Recipient interface {
	HandleNewOrder(request models.NewOrderForm) error
}

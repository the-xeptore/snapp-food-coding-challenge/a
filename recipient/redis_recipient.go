package recipient

import (
	"errors"
	"fmt"

	"github.com/adjust/rmq/v3"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/config"
)

type RedisRecipient struct {
	connection rmq.Connection
	queue      rmq.Queue
}

func InitializeRedisRecipient(conf *config.RecipientConfig) (Recipient, error) {
	connectionAddress := fmt.Sprintf("%s:%d", conf.RedisHost, conf.RedisPort)
	connection, err := rmq.OpenConnection(conf.RedisConnectionTag, "tcp", connectionAddress, 1, nil)
	if nil != err {
		return nil, errors.New(fmt.Sprintf("Could not connect to redis server: %s", err.Error()))
	}

	queue, err := connection.OpenQueue(conf.RedisQueueName)
	if nil != err {
		return nil, errors.New(fmt.Sprintf("Unable to open queue '%s': %s", conf.RedisQueueName, err.Error()))
	}

	return &RedisRecipient{connection, queue}, err
}

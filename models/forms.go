package models

type NewOrderForm struct {
	OrderID int
	Price   uint
	Title   string
}

package post_order

type form struct {
	OrderID int    `json:"order_id"`
	Price   uint   `json:"price"`
	Title   string `json:"title"`
}

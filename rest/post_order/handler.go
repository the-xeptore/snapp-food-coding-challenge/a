package post_order

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/models"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/recipient"
)

func CreateHandler(rec recipient.Recipient) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		body := form{}
		err := ctx.ShouldBindJSON(&body)
		if nil != err {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		model := models.NewOrderForm{
			OrderID: body.OrderID,
			Price:   body.Price,
			Title:   body.Title,
		}

		rec.HandleNewOrder(model)

		fmt.Printf("\n%#v\n", model)
	}
}

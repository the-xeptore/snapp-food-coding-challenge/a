package rest

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/config"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/recipient"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/rest/post_order"
)

type Server struct {
	rec recipient.Recipient
}

func (server *Server) wireRoutes(router *gin.Engine) {
	router.POST("/api/order", post_order.CreateHandler(server.rec))
}

func Initialize(rec recipient.Recipient) (Server, error) {
	return Server{rec}, nil
}

func (server *Server) Start(serverConfig *config.ServerConfig) error {
	if serverConfig.ReleaseMode {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.Default()

	server.wireRoutes(router)

	return router.Run(fmt.Sprintf("%s:%d", serverConfig.Host, serverConfig.Port))
}

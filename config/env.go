package config

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

type envConfig struct {
	orderPlacerDatabaseConfig OrderPlacerDatabaseConfig
}

func ensureEnvironmentVariablesExistence() error {
	requiredVariables := []string{
		"ORDER_PLACER_DATABASE_NAME",
		"ORDER_PLACER_DATABASE_HOST",
		"ORDER_PLACER_DATABASE_PORT",
		"ORDER_PLACER_DATABASE_USER",
		"ORDER_PLACER_DATABASE_PASSWORD",
	}
	for _, requiredVariable := range requiredVariables {
		_, exists := os.LookupEnv(requiredVariable)
		if !exists {
			return errors.New(fmt.Sprintf("environment variable '%s' is not provided", requiredVariable))
		}
	}

	return nil
}

func parseDbPort(portEnvironmentVariable string) (int, error) {
	return strconv.Atoi(portEnvironmentVariable)
}

func loadEnvConfig() (*envConfig, error) {
	err := godotenv.Load()
	if nil != err {
		return nil, errors.New(fmt.Sprintf("Error loading .env file variables: %s", err.Error()))
	}

	err = ensureEnvironmentVariablesExistence()
	if nil != err {
		return nil, errors.New(fmt.Sprintf("Required environment variables are not provided: %s", err.Error()))
	}

	dbPort, err := parseDbPort(os.Getenv("ORDER_PLACER_DATABASE_PORT"))

	cfg := &envConfig{
		orderPlacerDatabaseConfig: OrderPlacerDatabaseConfig{
			Name:     os.Getenv("ORDER_PLACER_DATABASE_NAME"),
			Host:     os.Getenv("ORDER_PLACER_DATABASE_HOST"),
			Port:     dbPort,
			User:     os.Getenv("ORDER_PLACER_DATABASE_USER"),
			Password: os.Getenv("ORDER_PLACER_DATABASE_PASSWORD"),
		},
	}

	return cfg, nil
}

package config

import (
	"errors"
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type fileConfig struct {
	Server struct {
		Port        uint   `yaml:"port"`
		Host        string `yaml:"host"`
		ReleaseMode bool   `yaml:"release_mode"`
	} `yaml:"server"`
	Recipient struct {
		RedisHost          string `yaml:"redis_host"`
		RedisPort          int    `yaml:"redis_port"`
		RedisConnectionTag string `yaml:"redis_connection_tag"`
		RedisQueueName     string `yaml:"redis_queue_name"`
	} `yaml:"recipient"`
	OrderPlacer struct {
		RedisHost          string `yaml:"redis_host"`
		RedisPort          int    `yaml:"redis_port"`
		RedisConnectionTag string `yaml:"redis_connection_tag"`
		RedisQueueName     string `yaml:"redis_queue_name"`
	} `yaml:"order_placer"`
}

func loadFileConfig() (*fileConfig, error) {
	configFile, err := os.Open("config.yaml")
	if nil != err {
		return nil, errors.New(fmt.Sprintf("Error opening configuration file: %s", err.Error()))
	}
	defer configFile.Close()

	cfg := &fileConfig{}
	decoder := yaml.NewDecoder(configFile)
	err = decoder.Decode(&cfg)
	if nil != err {
		return nil, errors.New(fmt.Sprintf("Error parsing configuration file content: %s", err.Error()))
	}

	return cfg, nil
}

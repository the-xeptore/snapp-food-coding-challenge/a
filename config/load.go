package config

type ServerConfig struct {
	Host        string
	Port        uint
	ReleaseMode bool
}

type RecipientConfig struct {
	RedisHost          string
	RedisPort          int
	RedisConnectionTag string
	RedisQueueName     string
}

type OrderPlacerDatabaseConfig struct {
	Name     string
	Host     string
	Port     int
	User     string
	Password string
}

type OrderPlacerConfig struct {
	RedisHost          string
	RedisPort          int
	RedisConnectionTag string
	RedisQueueName     string
	Database           OrderPlacerDatabaseConfig
}

type Config struct {
	Server      ServerConfig
	Recipient   RecipientConfig
	OrderPlacer OrderPlacerConfig
}

func Load() (*Config, error) {
	fileConfig, err := loadFileConfig()
	if nil != err {
		return nil, err
	}

	envConfig, err := loadEnvConfig()
	if nil != err {
		return nil, err
	}

	cfg := &Config{
		Server: ServerConfig{
			Host:        fileConfig.Server.Host,
			Port:        fileConfig.Server.Port,
			ReleaseMode: fileConfig.Server.ReleaseMode,
		},
		Recipient: RecipientConfig{
			RedisHost:          fileConfig.Recipient.RedisHost,
			RedisPort:          fileConfig.Recipient.RedisPort,
			RedisConnectionTag: fileConfig.Recipient.RedisConnectionTag,
			RedisQueueName:     fileConfig.Recipient.RedisQueueName,
		},
		OrderPlacer: OrderPlacerConfig{
			RedisHost:          fileConfig.OrderPlacer.RedisHost,
			RedisPort:          fileConfig.OrderPlacer.RedisPort,
			RedisConnectionTag: fileConfig.OrderPlacer.RedisConnectionTag,
			RedisQueueName:     fileConfig.OrderPlacer.RedisQueueName,
			Database:           envConfig.orderPlacerDatabaseConfig,
		},
	}

	return cfg, nil
}

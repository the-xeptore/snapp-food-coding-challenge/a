package order_placer

import (
	"github.com/adjust/rmq/v3"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/order_placer/store"
)

func createNewOrderHandler(store *store.Store) rmq.ConsumerFunc {
	return func(delivery rmq.Delivery) {
		delivery.Ack()

		return
	}
}

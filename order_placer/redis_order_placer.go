package order_placer

import (
	"errors"
	"fmt"
	"time"

	"github.com/adjust/rmq/v3"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/config"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/order_placer/store"
)

type RedisOrderPlacer struct {
	connection rmq.Connection
	queue      rmq.Queue
}

func InitializeRedisOrderPlacer(conf *config.OrderPlacerConfig, store *store.Store) error {
	connectionAddress := fmt.Sprintf("%s:%d", conf.RedisHost, conf.RedisPort)
	connection, err := rmq.OpenConnection(conf.RedisConnectionTag, "tcp", connectionAddress, 1, nil)
	if nil != err {
		return errors.New(fmt.Sprintf("Could not connect to redis server: %s", err.Error()))
	}

	queue, err := connection.OpenQueue(conf.RedisQueueName)
	if nil != err {
		return errors.New(fmt.Sprintf("Unable to open queue '%s': %s", conf.RedisQueueName, err.Error()))
	}

	err = queue.StartConsuming(1000, 100*time.Millisecond)
	if nil != err {
		return errors.New(fmt.Sprintf("Unable to start consuming queue '%s': %s", conf.RedisQueueName, err.Error()))
	}

	queue.AddConsumerFunc("handle-new-order", createNewOrderHandler(store))

	return err
}

package store

import (
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/models"
)

func (store *mysqlStore) SaveNewOrder(order *models.NewOrderForm) error {
	stmt, err := store.db.Prepare("INSERT INTO orders (order_id, price, title) VALUES (?, ?, ?)")
	if err != nil {
		// FIXME: do not propagate internal database error to upper layers!
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(order.OrderID, order.Price, order.Title)
	if err != nil {
		// FIXME: do not propagate internal database error to upper layers!
		return err
	}

	return nil
}

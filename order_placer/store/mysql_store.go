package store

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/config"
)

type mysqlStore struct {
	db *sql.DB
}

func InitializeMysqlStore(conf *config.OrderPlacerDatabaseConfig) (Store, error) {
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", conf.User, conf.Password, conf.Host, conf.Port, conf.Name)
	db, err := sql.Open("mysql", dataSourceName)
	if nil != err {
		// FIXME: replace with a more accurate custom error
		return nil, err
	}

	err = db.Ping()
	if nil != err {
		// FIXME: replace with a more accurate custom error
		return nil, err
	}

	return &mysqlStore{db}, err
}

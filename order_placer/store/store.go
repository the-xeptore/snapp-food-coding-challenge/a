package store

import (
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/a/models"
)

type Store interface {
	SaveNewOrder(order *models.NewOrderForm) error
}
